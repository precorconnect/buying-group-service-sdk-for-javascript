## Description
Precor Connect buying group service SDK for javascript.

## Features

## Setup

**install via jspm**  
```shell
jspm install buying-group-service-sdk=bitbucket:precorconnect/buying-group-service-sdk-for-javascript
``` 

**import & instantiate**
```javascript
import BuyingGroupServiceSdk,{BuyingGroupServiceSdkConfig} from 'buying-group-service-sdk'

const buyingGroupServiceSdkConfig =
    new BuyingGroupServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const buyingGroupServiceSdk =
    new BuyingGroupServiceSdk(
        buyingGroupServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```