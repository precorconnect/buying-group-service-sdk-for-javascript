/**
 * @class {BuyingGroupServiceSdkConfig}
 */
export default class BuyingGroupServiceSdkConfig{

    _precorConnectApiBaseUrl:string;


    /**
     * @param {string} precorConnectApiBaseUrl
     */
    constructor(precorConnectApiBaseUrl:string){

        if(!precorConnectApiBaseUrl) {
            throw 'precorConnectApiBaseUrl required';
        }
        this._precorConnectApiBaseUrl = precorConnectApiBaseUrl;


    }


    /**
     * @returns {string}
     */
    get precorConnectApiBaseUrl(){
        return this._precorConnectApiBaseUrl;
    }
}
