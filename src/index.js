/**
 * @module
 * @description buying group service sdk public API
 */
export {default as BuyingGroupServiceSdkConfig } from './buyingGroupServiceSdkConfig';
export {default as BuyingGroupSynopsisView} from './buyingGroupSynopsisView';
export {default as default} from './buyingGroupServiceSdk';