import config from './config';
import jwt from 'jwt-simple';
import dummy from '../dummy';

export default {
    constructValidAppAccessToken
}

    function constructValidAppAccessToken():string {

        const tenMinutesInMilliseconds = 10000 * 60;

        const jwtPayload = {
            "type": 'app',
            "exp": Date.now() + tenMinutesInMilliseconds,
            "aud": dummy.url,
            "iss": dummy.url
        };

        return jwt.encode(jwtPayload, config.identityServiceJwtSigningKey);

}
